import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class Ppm extends ImageDecipher{
	private byte[] loadPixels;
	byte[] inversionOfBytes;

//Path	
	Ppm(String path){
		this.setPath(path);
	}

//Read Image	
	public BufferedImage readImage(){
	  try {
		FileInputStream file = new FileInputStream(path);
		int counter = 0;
		String line;
			
		line = Ppm.readLine(file);
			if("P6".equals(line)){
				line = Ppm.readLine(file);
				
			while(line.startsWith("#")){
					line = Ppm.readLine(file);
				}
				Scanner sizes = new Scanner (line);
			if(sizes.hasNext() && sizes.hasNextInt()){
					width = sizes.nextInt();
				}
			if(sizes.hasNext() && sizes.hasNextInt()){
					height = sizes.nextInt();
				}
				sizes.close();
				
		line = Ppm.readLine(file);
		sizes = new Scanner(line);
		maxColor = sizes.nextInt();
	    sizes.close();
				
	     imageIn = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
               int i,j,a;
			   j=2;
			   a=j;
				
			    loadPixels=new byte[height * width * 3];
				for(i=0;i<loadPixels.length;i++){
					loadPixels[i]=(byte)file.read();
					
				}
				inversionOfBytes = new byte[loadPixels.length];
		        byte[] pixels = new byte[loadPixels.length];
		        for (i = 0; i < loadPixels.length; i++) {
		            inversionOfBytes[j] = (byte) loadPixels[i];
		            j--;
		            if ((i + 1) % 3 == 0) {
		                j = a + 3;
		                a += 3;
		            }
		        }
		        pixels = ((DataBufferByte) imageIn.getRaster().getDataBuffer()).getData();
		    while(counter < 3 * width * height){
					pixels[counter] = (byte) inversionOfBytes[counter];
					counter++;
				}
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return imageIn;
	}

//RED filter	
	public BufferedImage redFilter(){
		BufferedImage redImage = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
		int counter = 0;
		while(counter < inversionOfBytes.length){
			if((1+counter) % 3 != 0)
				inversionOfBytes[counter] = 0;
			counter++;
		}
		Pixels = ((DataBufferByte) redImage.getRaster().getDataBuffer()).getData();
        counter = 0;
		while(counter < 3 * width * height){
			Pixels[counter] = (byte) inversionOfBytes[counter];
			counter++;
		}
		return redImage;
	}

//GREEN filter	
	public BufferedImage greenFilter(){
		BufferedImage greenImage = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
		int counter = 0;
		while(counter < inversionOfBytes.length){
			if((2 + counter) % 3 != 0)
				inversionOfBytes[counter] = 0;
			counter ++;
		}
		Pixels = ((DataBufferByte) greenImage.getRaster().getDataBuffer()).getData();
        counter = 0;
		while(counter < 3 * width * height){
			Pixels[counter] = (byte) inversionOfBytes[counter];
			counter ++;
		}
		return greenImage;
	}

//BLUE filter	
	public BufferedImage blueFilter(){
		BufferedImage blueImage = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
		int counter = 0;
		while(counter < inversionOfBytes.length){
			if((counter) % 3 != 0)
				inversionOfBytes[counter] = 0;
			counter++;
		}
		Pixels = ((DataBufferByte) blueImage.getRaster().getDataBuffer()).getData();
        counter = 0;
		while(counter < 3 * width * height){
			Pixels[counter] = (byte) inversionOfBytes[counter];
			counter ++;
		}
		return blueImage;
	}

//NEGATIVE filter	
	public BufferedImage negativeFilter(){
		BufferedImage negativeImage = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
		int counter = 0;
		while(counter < inversionOfBytes.length){
			inversionOfBytes[counter] = (byte) (maxColor-inversionOfBytes[counter]);
			counter++;
		}
		Pixels = ((DataBufferByte) negativeImage.getRaster().getDataBuffer()).getData();
        counter = 0;
		while(counter < 3 * width * height){
			Pixels[counter] = (byte) inversionOfBytes[counter];
			counter++;
		}
		return negativeImage;
}

	@Override
	String Hidden_Message(String filePath) throws FileNotFoundException, IOException {
		return null;
	}
}
