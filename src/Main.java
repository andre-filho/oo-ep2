import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Image;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;

public class Main  {

	public static void main(String[] args) throws FileNotFoundException, IOException {

		JFileChooser chooser = new JFileChooser();
		chooser.setDialogTitle("Select the image file");
		File workingDirectory = new File(System.getProperty("user.dir"));
		chooser.setCurrentDirectory(workingDirectory);
		int returnVal = chooser.showOpenDialog(null);
		
		Pgm teste = new Pgm(chooser.getSelectedFile().getAbsolutePath());

	
		Pgm image1 = new Pgm(chooser.getSelectedFile().getAbsolutePath());
		Ppm secret = new Ppm(chooser.getSelectedFile().getAbsolutePath());


		image1.readImage();
		JFrame frame = new JFrame("Filtered Image");
		frame.getContentPane().setBackground(Color.GRAY);
		secret.readImage();
		
		frame.getContentPane().setLayout(new FlowLayout());
		
		frame.getContentPane().add(new JLabel(new ImageIcon(image1.readImage().getScaledInstance(300, 300, Image.SCALE_DEFAULT))));
		frame.getContentPane().add(new JLabel(new ImageIcon(image1.negativeFilter().getScaledInstance(300, 300, Image.SCALE_DEFAULT))));
		frame.getContentPane().add(new JLabel(new ImageIcon(image1.sharpenFilter().getScaledInstance(300, 300, Image.SCALE_DEFAULT))));
		frame.getContentPane().add(new JLabel(new ImageIcon(image1.blurFilter().getScaledInstance(300, 300, Image.SCALE_DEFAULT))));	

		
//		frame.getContentPane().add(new JLabel(new ImageIcon(secret.redFilter().getScaledInstance(400, 400, Image.SCALE_DEFAULT))));
//		frame.getContentPane().add(new JLabel(new ImageIcon(secret.blueFilter().getScaledInstance(400, 400, Image.SCALE_DEFAULT))));
//		frame.getContentPane().add(new JLabel(new ImageIcon(secret.greenFilter().getScaledInstance(400, 400, Image.SCALE_DEFAULT))));
//		frame.getContentPane().add(new JLabel(new ImageIcon(secret.negativeFilter().getScaledInstance(400, 400, Image.SCALE_DEFAULT))));
			
		JFrame frame2 = new JFrame("Hidden Message");
		frame2.getContentPane().setBackground(Color.GRAY);
		JList lista;
		DefaultListModel modelo = new DefaultListModel();
		lista = new JList(modelo);
		lista.setVisibleRowCount(500);
		modelo.addElement(teste.Hidden_Message(chooser.getSelectedFile().getAbsolutePath()));
		frame2.getContentPane().add(lista);
        frame2.pack();
        frame2.setVisible(true);
		
        frame.pack();
		frame.setVisible(true);
	}
}
