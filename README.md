********************************************************************************
Universidade de Brasília - Faculdade do Gama - FGA
Orientação a Objetos - 2016.1 - Turma B - Profº Renato Sampaio
Autor: André de Sousa Costa Filho - 15/0005521
EXERCÍCIO DE PROGRAMAÇÃO #02
********************************************************************************

O exercicio de programação funciona da seguinte maneira:

1- selecione para rodar a classe Main();
2- selecione a imagem pgm primeiro.
3- serao geradas duas janelas. uma com a mensagem e outra com as imagens (foram redimensionadas e aparecem com todos os filtros)
4- para fazer com a ppm a aplicação dos filtros, comente as linhas  39-42, 50-59 e os filtros não desejados.